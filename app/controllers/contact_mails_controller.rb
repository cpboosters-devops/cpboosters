class ContactMailsController < ApplicationController
    def contact
        if (create_params.present?)

          ContactMailer.gratitude(create_params[:email]).deliver
          ContactMailer.new_client(create_params).deliver

          render json: { message: "Success" }, code: :ok

        else

          render json: { message: "Missing params" }, code: :unprocessable_entity
        end

    end

    private

    def create_params
        params.permit(
            :name,
            :last_name,
            :email,
            :phone,
            :rol,
            :company,
            :employees,
            :service_type,
            :country,
            :message
        )
    end
end

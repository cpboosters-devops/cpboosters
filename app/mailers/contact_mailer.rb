class ContactMailer < ApplicationMailer

  def gratitude(addressee)
    return unless addressee.present?

    mail(
      to: addressee, 
      subject: "Gracias por ponerte en contacto con Capability Boosters",
      body: gratitude_message
    )
  end

  def new_client(params)

    mail(
      to: "mkilic@cpboosters.com",
      subject: "Hola, soy un nuevo cliente",
      body: new_client_message(params)
    )
  
  end

  def gratitude_message
    <<-TEXT 
    Hola, gracias por contactarnos. En breve, una persona de nuestro equipo se contactará contigo para atenderte y resolver tus dudas. Saludos.
    TEXT
  end

  def new_client_message(params)
    <<-TEXT
    Hola, mi nombre es: #{params[:name]} #{params[:last_name]}, mis datos donde pueden contactarme son
    el siguiente correo electronico: #{params[:email]}, el siguiente numero telefonico: #{params[:phone]},
    el rol que presento en la empresa es: #{params[:rol]}, en la empresa: #{params[:company]},
    el numero de empleados es de aproximadamente: #{params[:employees]}, el tipo de servicio es #{params[:service_type]},
    el pais es: #{params[:country]}, y tengo el siguiente mensaje
    #{params[:message]}
    TEXT
  end
end

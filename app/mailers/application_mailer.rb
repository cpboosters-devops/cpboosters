class ApplicationMailer < ActionMailer::Base
  default from: 'hola@cpboosters.com'
  layout 'mailer'
end
